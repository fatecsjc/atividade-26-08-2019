package br.edu.fatecsjc;

import java.util.Date;

public class Professor extends Usuario {

    private String registroProfessor;
    private String departamento;

    public Professor() {
    }

    public Professor(String nome, String endereco, String cpf, Date dataNascimento, String registroProfessor, String departamento) {
        super(nome, endereco, cpf, dataNascimento);
        this.registroProfessor = registroProfessor;
        this.departamento = departamento;
    }

    public String getRegistroProfessor() {
        return registroProfessor;
    }

    public void setRegistroProfessor(String registroProfessor) {
        this.registroProfessor = registroProfessor;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }
}
