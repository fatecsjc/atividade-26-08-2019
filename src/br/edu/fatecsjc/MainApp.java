package br.edu.fatecsjc;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainApp {

    public static void main(String[] args) {

        Aluno aluno1 = new Aluno();
        aluno1.setNome("Robson");
        aluno1.setCurso("Banco de Dados");
        aluno1.setRa("1460281823042");
        aluno1.setCpf("275864828-88");
        aluno1.setDataNascimento(new Date(1979, 8, 26));

        Professor professor1 = new Professor();
        professor1.setNome("Lucas");
        professor1.setDepartamento("Tecnologia da Informação");
        professor1.setRegistroProfessor("4242");
        professor1.setCpf("42424242");
        professor1.setEndereco("Rua das Acácias");
        professor1.setDataNascimento(new Date(1985, 7, 22));


        List<Usuario> usuarios = new ArrayList<>();

        usuarios.add(aluno1);
        usuarios.add(professor1);

        for (Usuario u : usuarios){
            System.out.println(u.getNome());
        }
    }
}
