package br.edu.fatecsjc;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Funcionario extends Usuario {

    private Date dataAdmissao;
    private String departamento;

    public Funcionario() {
    }

    public Funcionario(String nome, String endereco, String cpf, Date dataNascimento, Date dataAdmissao, String departamento) {
        super(nome, endereco, cpf, dataNascimento);
        this.dataAdmissao = dataAdmissao;
        this.departamento = departamento;
    }

    public Date getDataAdmissao() {
        return dataAdmissao;
    }

    public void setDataAdmissao(Date dataAdmissao) {
        this.dataAdmissao = dataAdmissao;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public void cadastrarObra(Obra obra) {
        List<Obra> obras = new ArrayList<>();
        obras.add(obra);
    }
}
