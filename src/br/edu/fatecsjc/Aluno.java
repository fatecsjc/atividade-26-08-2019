package br.edu.fatecsjc;

import java.util.Date;

public class Aluno extends Usuario {

    private String ra;
    private String curso;

    public Aluno() {
    }

    public Aluno(String nome, String endereco, String cpf, Date dataNascimento, String ra, String curso) {
        super(nome, endereco, cpf, dataNascimento);
        this.ra = ra;
        this.curso = curso;
    }

    public String getRa() {
        return ra;
    }

    public void setRa(String ra) {
        this.ra = ra;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }
}
